"""Color chooser implementing (almost) the tkColorColor interface
"""

import os

import Main
import ColorDB


class Chooser:
    """Ask for a color"""

    def __init__(
        self,
        master=None,
        databasefile=None,
        initfile=None,
        ignore=None,
        wantspec=None,
    ):
        self._master = master
        self._databasefile = databasefile
        self._initfile = initfile or os.path.expanduser('~/.pynche')
        self._ignore = ignore
        self._pw = None
        self._wantspec = wantspec

    def show(self, color, options):
        # Scan for options that can override the ctor options.
        self._wantspec = options.get('wantspec', self._wantspec)
        dbfile = options.get('databasefile', self._databasefile)
        # Load the database file.
        colordb = None
        if dbfile != self._databasefile:
            colordb = ColorDB.get_colordb(dbfile)
        if not self._master:
            from tkinter import Tk

            self._master = Tk()
        if not self._pw:
            self._pw, self._sb = Main.build(
                master=self._master,
                initfile=self._initfile,
                ignore=self._ignore,
            )
        else:
            self._pw.deiconify()
        # Convert color.
        if colordb:
            self._sb.set_colordb(colordb)
        else:
            colordb = self._sb.colordb()
        if color:
            r, g, b = Main.initial_color(color, colordb)
            self._sb.update_views(r, g, b)
        # Reset the canceled flag and run it.
        self._sb.canceled = False
        Main.run(self._pw, self._sb)
        rgbtuple = self._sb.current_rgb()
        self._pw.withdraw()
        # Check to see if the cancel button was pushed.
        if self._sb.canceled:
            return None, None
        # Try to return the color name from the database if there is an exact
        # match, otherwise use the "#rrggbb" spec.  BAW: Forget about color
        # aliases for now, maybe later we should return these too.
        name = None
        if not self._wantspec:
            try:
                name = colordb.find_byrgb(rgbtuple)[0]
            except ColorDB.BadColor:
                pass
        if name is None:
            name = ColorDB.triplet_to_rrggbb(rgbtuple)
        return rgbtuple, name

    def save(self):
        if self._sb:
            self._sb.save_views()


# For convenience.
_chooser = None


def askcolor(color=None, **options):
    """Ask for a color"""
    global _chooser
    if not _chooser:
        _chooser = Chooser(**options)
    return _chooser.show(color, options)


def save():
    global _chooser
    if _chooser:
        _chooser.save()


# Testing
if __name__ == '__main__':
    from tkinter import Button, Label, Tk

    class Tester:
        def __init__(self):
            self._root = tk = Tk()
            b = Button(tk, text='Choose Color...', command=self._choose)
            b.pack()
            self._l = Label(tk)
            self._l.pack()
            q = Button(tk, text='Quit', command=self._quit)
            q.pack()

        def _choose(self, event=None):
            rgb, name = askcolor(master=self._root)
            if rgb is None:
                text = 'You hit CANCEL!'
            else:
                r, g, b = rgb
                text = 'You picked %s (%3d/%3d/%3d)' % (name, r, g, b)
            self._l.configure(text=text)

        def _quit(self, event=None):
            self._root.quit()

        def run(self):
            self._root.mainloop()

    t = Tester()
    t.run()
    # simpler
    print(f'color: {askcolor()}')
    print(f'color: {askcolor()}')
